<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_db extends CI_Model{

    public function create($table,$data){

        return $this->db->insert($table,$data);

    }

    public function update($table,$data,$id,$column){

        return  $this->db->where($column,$id)->update($table,$data);

    }
    public function destroy($table,$id,$column){

        return $this->db->where($column,$id)->delete($table);

    }

    public function selectRow($table,$id,$column){
        return $this->db->where($column,$id)->get($table)->row();
    }

    public function selectAllRow($table){
        return $this->db->get($table)->result();
    }
}

