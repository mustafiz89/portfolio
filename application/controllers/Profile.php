<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
Class Profile extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }
    protected $table='profile';
    protected $column='profile_id';

    public function index(){
        $data['title']='View Profile Information';
        $data['data']=$this->Model_db->selectRow($this->table,$id=1,$this->column);
        $data['content']=$this->load->view('profile/manage-profile',$data,true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function add(){
        $data['title']='Add Profile Information';
        $data['content']=$this->load->view('profile/add-profile',$data,true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function store(){
        $input=array(
            'fullname'=>$this->input->post('fullname',true),
            'nickname'=>$this->input->post('nickname',true),
            'age'=>$this->input->post('age',true),
            'address'=>$this->input->post('address',true),
            'email'=>$this->input->post('email',true),
            'phone'=>$this->input->post('phone',true),
            'freelance'=>$this->input->post('freelance',true),
            'title'=>$this->input->post('title',true),
            'description'=>$this->input->post('description',true),
        );
        $this->Model_db->create($input,$this->table);
        redirect('profile/add');
    }

    public function view(){
        $data['title']='View Profile Information';
        $data['data']=$this->Model_db->selectRow($this->table,$id=1,$this->column);
        $data['content']=$this->load->view('profile/manage-profile',$data,true);
        $this->load->view('layouts/admin-master',$data);
    }
    public function edit(){
        $id=$this->uri->segment(3);
        $data['title']='Edit Profile Information';
        $data['data']=$this->Model_db->selectRow($this->table,$id,$this->column);
        $data['content']=$this->load->view('profile/edit-profile',$data,true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function update(){
        $id=$this->input->post('id',true);
        $input=array(
            'fullname'=>$this->input->post('fullname',true),
            'nickname'=>$this->input->post('nickname',true),
            'age'=>$this->input->post('age',true),
            'address'=>$this->input->post('address',true),
            'email'=>$this->input->post('email',true),
            'phone'=>$this->input->post('phone',true),
            'freelance'=>$this->input->post('freelance',true),
            'title'=>$this->input->post('title',true),
            'description'=>$this->input->post('description',true),
        );
        $this->Model_db->update($this->table,$input,$id,$this->column);
        $this->session->set_flashdata('message','Date Update Successfully');
        redirect('profile');
    }
    public function destroy($id){
        $this->Model_db->destroy($this->table,$id,$this->column);
        redirect('profile');
    }
}
