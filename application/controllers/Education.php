<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
Class Education extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }
    protected $table='education';
    protected $column='education_id';

    public function view($data,$content){
        $data['content']=$this->load->view($content,$data,true);
        $this->load->view('layouts/admin-master',$data);
    }


    public function index(){
        $data['title']='All Education Information';
        $data['allData']=$this->Model_db->selectAllRow($this->table);
        $this->view($data,'education/manage-education');
    }
    public function add(){
        $data['title']='Add Education Information';
        $this->view($data,'education/add-education');
    }
    public function store(){
        $input=array(
            'degree'=>$this->input->post('degree',true),
            'institute'=>$this->input->post('institute',true),
            'result'=>$this->input->post('result',true),
            'group'=>$this->input->post('group',true),
            'subject'=>$this->input->post('subject',true),
            'board'=>$this->input->post('board',true),
            'duration'=>$this->input->post('duration',true)
        );
        $this->Model_db->create($this->table,$input);
        $this->session->set_flashdata('message','Data add successfully');
        redirect('education/add');
    }

    public function edit(){
    $id=$this->uri->segment(3);
        $data['title']='Edit Education Information';
        $data['data']=$this->Model_db->selectRow($this->table,$id,$this->column);
        $this->view($data,'education/edit-education');
    }

    public function update(){
       $id=$this->input->post('id');
        $input=array(
            'degree'=>$this->input->post('degree',true),
            'institute'=>$this->input->post('institute',true),
            'result'=>$this->input->post('result',true),
            'group'=>$this->input->post('group',true),
            'subject'=>$this->input->post('subject',true),
            'board'=>$this->input->post('board',true),
            'duration'=>$this->input->post('duration',true)
        );
        $this->Model_db->update($this->table,$input,$id,$this->column);
        $this->session->set_flashdata('message','Data Update successfully');
        redirect('education/index');
    }

    public function destroy(){
     $id=$this->uri->segment(3);
        $this->Model_db->destroy($this->table,$id,$this->column);
        $this->session->set_flashdata('message','Data Delete successfully');
        redirect('education/index');
    }
}