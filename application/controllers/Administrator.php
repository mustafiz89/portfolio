<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
Class Administrator extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }

    public function index(){
        $data['content']=$this->load->view('layouts/dashboard','',true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function otherView(){

    }
}
