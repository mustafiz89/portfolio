<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
Class Training extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }

    protected $table='training';
    protected $column='training_id';

    public function view($data,$content){
        $data['content']=$this->load->view($content,$data,true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function index(){

        $data['title']='View Professional Training Experience';
        $data['data']=$this->Model_db->selectAllRow($this->table);
        $this->view($data,'training/manage-training-experience');
    }

    public function add(){
        $data['title']='Add Professional training experience';
        $this->view($data,'training/add-training-experience');
    }

    public function store(){
        $input=array(
            'title'=>$this->input->post('title'),
            'organization'=>$this->input->post('organization'),
            'duration'=>$this->input->post('duration'),
            'description'=>$this->input->post('description')
        );
        $this->Model_db->create($this->table,$input);
        $this->session->set_flashdata('message','Data Save Successfully');
        redirect('training/add');
    }

    public function edit(){

        $id=$this->uri->segment(3);
        $data['title']='Edit Professional training experience';
        $data['data']=$this->Model_db->selectRow($this->table,$id,$this->column);
        $this->view($data,'training/edit-training-experience');

    }

    public function update(){
        $id=$this->input->post('id');
        $input=array(
            'title'=>$this->input->post('title'),
            'organization'=>$this->input->post('organization'),
            'duration'=>$this->input->post('duration'),
            'description'=>$this->input->post('description')
        );
        $this->Model_db->update($this->table,$input,$id,$this->column);
        $this->session->set_flashdata('message','Data Update successfully');
        redirect('training');
    }

    public function destroy(){
        $id=$this->uri->segment(3);
        $this->Model_db->destroy($this->table,$id,$this->column);
        $this->session->set_flashdata('message','Data Delete successfully');
        redirect('training');
    }

}
