<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//session_start();
Class Experience extends CI_Controller{
    public function __construct(){
        parent::__construct();

    }

    protected $table='experience';
    protected $column='experience_id';

    public function view($data,$content){
        $data['content']=$this->load->view($content,$data,true);
        $this->load->view('layouts/admin-master',$data);
    }

    public function index(){

        $data['title']='View Professional Experience';
        $data['data']=$this->Model_db->selectAllRow($this->table);
        $this->view($data,'experience/manage-experience');
    }

    public function add(){
        $data['title']='Add Professional Experience';
        $this->view($data,'experience/add-experience');
    }

    public function store(){
        $input=array(
            'title'=>$this->input->post('title'),
            'company'=>$this->input->post('company'),
            'duration'=>$this->input->post('duration'),
            'description'=>$this->input->post('description')
        );
        $this->Model_db->create($this->table,$input);
        $this->session->set_flashdata('message','Data Save Successfully');
        redirect('experience/add');
    }

    public function edit(){

        $id=$this->uri->segment(3);
        $data['title']='Edit Professional Experience';
        $data['data']=$this->Model_db->selectRow($this->table,$id,$this->column);
        $this->view($data,'experience/edit-experience');

    }

    public function update(){
        $id=$this->input->post('id');
        $input=array(
            'title'=>$this->input->post('title'),
            'company'=>$this->input->post('company'),
            'duration'=>$this->input->post('duration'),
            'description'=>$this->input->post('description')
        );
        $this->Model_db->update($this->table,$input,$id,$this->column);
        $this->session->set_flashdata('message','Data Update successfully');
        redirect('experience');
    }

    public function destroy(){
        $id=$this->uri->segment(3);
        $this->Model_db->destroy($this->table,$id,$this->column);
        $this->session->set_flashdata('message','Data Delete successfully');
        redirect('experience');
    }

}
