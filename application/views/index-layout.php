<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Mustafizur Rahman</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>

    <link rel="shortcut icon" href="favicon.html"/>
    <link href='http://fonts.googleapis.com/css?family=Patua+One|Montserrat|Open+Sans:400,700,600' rel='stylesheet' type='text/css'/>
    <link href="<?=base_url();?>assets/site/js/fancybox/jquery.fancybox8cbb.css?v=2.1.5" rel="stylesheet" type="text/css" media="screen"/>
    <link href="<?=base_url();?>assets/site/js/mediaelement/mediaelementplayer.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/css/settings_style.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/css/style-print.css" type="text/css" media="print" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/css/skin_style.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/style.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/css/color_cheme.css" rel="stylesheet"/>
    <link href="<?=base_url();?>assets/site/css/responsive.css" rel="stylesheet"/>

    <script src="<?=base_url();?>assets/site/js/jquery-1.10.2.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?=base_url();?>assets/site/js/html5shiv.js"></script>
    <script src="<?=base_url();?>assets/site/js/respond.min.js"></script>
    <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <![endif]-->
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/detect_mobilebrowser_and_ipad.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/fancybox/jquery.fancybox.pack8cbb.js?v=2.1.5"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/jquery.slides.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/mediaelement/mediaelement-and-player.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/jquery.cookies.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/jquery.form.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/site/js/main.js"></script>

    <style>
        p{
            text-align: justify;
        }
        .profile-info p{
            font-size: 14px;
        }
    </style>
</head>

<body class="bg_f0f2f2 color_53b7f9 light_skin">
<div class="row-wrap wrapper light_skin">
    <div class="row-space">

        <header id="header" class="row noprint">
            <h1 class="head-name">Mustafizur Rahman</h1>

            <div class="head-social">
                <ul>

                    <li class="fb"><a href="https://www.facebook.com/mustafizurrahman.saahir" target="_blank">Facebook</a></li>
                    <li class="lnkd"><a href="https://www.linkedin.com/" target="_blank">Linkedin</a></li>
                    <li class="gplus"><a href="https://plus.google.com/" target="_blank">Google+</a></li>
                    <li class="drb"><a href="http://dribbble.com/" target="_blank">Dribble</a></li>
                    <li class="inst"><a href="http://instagram.com/" target="_blank">Instagram</a></li>
                    <li class="tw"><a href="https://twitter.com/" target="_blank">Twitter</a></li>
                </ul>
            </div>
        </header>

        <section id="profile" class="item noprint">
            <h2 class="item-title"><span class="title">my Profile</span> <span class="icon-user"></span></h2>

            <div class="item-cont clearfix">
                <div class="hidden">
                    <div class="col500 clearfix fl-left">
                        <div class="profile-img"><img src="<?=base_url();?>assets/site/images/user_images/1.jpg" alt=""/></div>
                        <div class="profile-info">
                            <h3>I'm a Professional Software & Web Developer in Bangladesh.</h3>

                            <p>I have always been an achiever; be it academic or professional life or sports or any
                                other field in my life.</p>
                            <p>I believe in success  through hardwork & dedication. My motto in life is to "If you want
                                something & work hard, You will achieve it. No shortcut." I enjoy life to the fullest & love humor.
                                I am progressive thinker & respect each person space & value. </p>
                        </div>
                    </div>

                    <div class="col260 fl-right">
                        <ul class="profile-data">
                            <li><h4>Full Name</h4>

                                <div>Mustafizur Rahman</div>
                            </li>
                            <li><h4>Nick name</h4>

                                <div>Saahir</div>
                            </li>
                            <li><h4> Age &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</h4>

                                <div>27</div>
                            </li>
                            <li><h4>Address&nbsp&nbsp&nbsp</h4>

                                <div>East Nandipara, Khilgoan, Dhaka.</div>
                            </li>
                            <li><h4>E-mail &nbsp &nbsp &nbsp &nbsp</h4>

                                <div>sohag2882@yahoo.com</div>
                            </li>
                            <li><h4>phone &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</h4>

                                <div>+8801556984227</div>
                            </li>
                            <li><h4>freelance</h4>

                                <div>Available</div>
                            </li>
                        </ul>
                    </div>
                </div>
                <a class="blog-link" href="">
                    <span class="icon"></span>
                    <span class="label">MY BLOG</span>
                </a>
            </div>
            <div id="profile-brd" class="item-border"><span><span></span></span></div>
        </section>
        <!-- /#profile -->

        <section id="resume" class="item">
            <h2 class="item-title toggle noprint closed"><span class="title">resume</span> <span
                    class="icon-resume"></span><span class="arrow"></span></h2>

            <div class="item-cont clearfix">
                <div class="col500 fl-left">
                    <div class="resume-category">
                        <h3 class="resume-category-title clearfix">EDUCATION <span class="icon-plus"></span></h3>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2002 - 2004</div>
                                <h4 class="resume-post-title">Secondary School Certificate (S.S.C)</h4>
                                <h5 class="resume-post-subtitle">P.T.G.D Govt High School</h5>

                                <div class="resume-post-cont">
                                    <p>Result : 3.81 ( Out of 5) </p>
                                    <p>Group  : Science</p>
                                    <p>Board  : Dhaka Board</p>
                                </div>
                            </div>
                        </div>
                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2004 - 2006</div>
                                <h4 class="resume-post-title">Higher Secondary School Certificate (H.S.C)</h4>
                                <h5 class="resume-post-subtitle">Shariatpur Govt Collage</h5>

                                <div class="resume-post-cont">
                                    <p>Result : 4.00 ( Out of 5) </p>
                                    <p>Group  : Science</p>
                                    <p>Board  : Dhaka Board</p>
                                </div>
                            </div>
                        </div>
                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2008 - 2012</div>
                                <h4 class="resume-post-title">B.Sc. ( Honor's ) in Computer Science & Engineering</h4>
                                <h5 class="resume-post-subtitle">Institute Of Science & Technology</h5>

                                <div class="resume-post-cont">
                                    <p>Result  : 3.00 ( Out of 4) </p>
                                    <p>Subject : Computer Science & Engineering</p>
                                    <p>Board   : National University</p>
                                </div>
                            </div>
                        </div>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2015 - Present</div>
                                <h4 class="resume-post-title">M.Sc in Computer Science & Engineering</h4>
                                <h5 class="resume-post-subtitle">Institute Of Science & Technology</h5>

                                <div class="resume-post-cont">
                                    <p>Result  : Appeard </p>
                                    <p>Subject : Computer Science & Engineering</p>
                                    <p>Board   : National University</p>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="resume-category">
                        <h3 class="resume-category-title clearfix">Professional experience <span class="icon-plus"></span></h3>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2014 - Present </div>
                                <h4 class="resume-post-title">Software Engineer</h4>
                                <h5 class="resume-post-subtitle">Dynamic Software Ltd</h5>

                                <div class="resume-post-cont">
                                    <p>Dynamic Software Ltd is one of the top leading software company in bangladesh
                                        where my responsibility is developing  and maintaining various kind of software and also software testing.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">2013 - 2014</div>
                                <h4 class="resume-post-title"> Web Developer</h4>
                                <h5 class="resume-post-subtitle">Uniquee Webers</h5>

                                <div class="resume-post-cont">
                                    <p>
                                        Unique weber is software solution ltd company in bangladesh where my responsibility was to develop web site and
                                        web based application for particular business organization and also web support.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


<!--                    {{-------------------------------------------------------------------}}-->


                    <div class="resume-category">
                        <h3 class="resume-category-title clearfix">Professional Training Experience <span
                                class="icon-plus"></span></h3>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date">April 2014 - July 2014</div>
                                <h4 class="resume-post-title">PHP( CODEIGNITER ) MVC FRAMEWORK</h4>
                                <h5 class="resume-post-subtitle">BASIS Institute of Technology & Management (BITM)</h5>

                                <div class="resume-post-cont">
                                    <p>After completed this course, My course result is grade A  and also get a certificate
                                        form BITM. I have completed several web application like dynamic website, web portal,
                                        school management software , e-commerce using php codeigniter framework.</p>
                                </div>
                            </div>
                        </div>

                        <div class="resume-post">
                            <div class="resume-post-body">
                                <div class="resume-post-date"> Novembar 2013 - March 2014</div>
                                <h4 class="resume-post-title">C# AND MICROSOFT SQL SERVER 2008</h4>
                                <h5 class="resume-post-subtitle">Ref: MD. Asaduzzaman Arif (MCT)</h5>
                                <h6 class="resume-post-subtitle">New Horizon CLC</h6>

                                <div class="resume-post-cont">
                                    <p>After successfully complete my C# .Net Course, I have completed several Desktop Software Project
                                        named Document Management System (DMS), Prize Bond Management System(PBMS), Hospital Management System (HMS)
                                        & Smart POS where i use C# Programing Language and Microsoft SQL Server 2008 and Microsoft Access Database. </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="resume-btns noprint" id="resume-btns">
                        <a target="_blank" href="" class="btn"><i class="icon-cloud"></i>Download resume</a>
<!--                        <a id="printBtn" onclick="window.print();return false;" target="_blank" href="#" class="btn">-->
<!--                            <i class="icon-print"></i>print resume</a>-->
                    </div>
                </div>

                <div class="col260 fl-right noprint">
                    <div class="resume-sidebar">
                        <aside class="skill-box">
                            <h3>WEB DESIGN SKILLS</h3>

                            <div class="textwidget">
                                <div class="skill-row">
                                    <h4 class="skill-title">HTML</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="80"></span>
                                        <span class="skill-percent">80%</span>
                                    </div>
                                </div>

                                <div class="skill-row">
                                    <h4 class="skill-title">CSS</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="70"></span>
                                        <span class="skill-percent">70%</span>
                                    </div>
                                </div>

                                <div class="skill-row">
                                    <h4 class="skill-title">BOOTSTRAP CSS</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="75"></span>
                                        <span class="skill-percent">75%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">WORDPRESS</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="40"></span>
                                        <span class="skill-percent">40%</span>
                                    </div>
                                </div>
                            </div>
                        </aside>

                        <aside class="skill-box">
                            <h3>WEB LANGUAGE SKILLS</h3>

                            <div class="textwidget">
                                <div class="skill-row">
                                    <h4 class="skill-title">ASP DOT NET</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="45"></span>
                                        <span class="skill-percent">45%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">PHP</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="60"></span>
                                        <span class="skill-percent">60%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">CODEIGNITER ( PHP FRAMEWORK )</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="70"></span>
                                        <span class="skill-percent">70%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">LARAVEL ( PHP FRAMEWORK )</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="40"></span>
                                        <span class="skill-percent">40%</span>
                                    </div>
                                </div>

                                <div class="skill-row">
                                    <h4 class="skill-title">JQUERY</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="60"></span>
                                        <span class="skill-percent">60%</span>
                                    </div>
                                </div>

                                <div class="skill-row">
                                    <h4 class="skill-title">JQUERY AJAX</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="75"></span>
                                        <span class="skill-percent">75%</span>
                                    </div>
                                </div>
                            </div>
                        </aside>

<!--                        {{---------------------------------------------}}-->

                        <aside class="skill-box">
                            <h3>PROGRAMMING LANGUAGE</h3>

                            <div class="textwidget">
                                <div class="skill-row">
                                    <h4 class="skill-title">C</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="45"></span>
                                        <span class="skill-percent">45%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">C++</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="55"></span>
                                        <span class="skill-percent">55%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">C#</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="70"></span>
                                        <span class="skill-percent">70%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">JAVA</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="20"></span>
                                        <span class="skill-percent">20%</span>
                                    </div>
                                </div>
                            </div>
                        </aside>

<!--                        {{--------------------------------------------------------------------}}-->
                        <aside class="skill-box">
                            <h3>SQL LANGUAGE SKILLS</h3>

                            <div class="textwidget">
                                <div class="skill-row">
                                    <h4 class="skill-title">MYSQL</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="65"></span>
                                        <span class="skill-percent">65%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">MICROSOFT SQL SERVER 2008</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="55"></span>
                                        <span class="skill-percent">55%</span>
                                    </div>
                                </div>

                                <div class="skill-row">
                                    <h4 class="skill-title">MICROSOFT ACCESS</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="50"></span>
                                        <span class="skill-percent">50%</span>
                                    </div>
                                </div>
                                <div class="skill-row">
                                    <h4 class="skill-title">MONGO DB</h4>

                                    <div class="skill-data">
                                        <span class="skill-percent-line" data-width="20"></span>
                                        <span class="skill-percent">20%</span>
                                    </div>
                                </div>
                            </div>
                        </aside>
<!--                        {{-------------------------------------------------------------------}}-->
                        <aside class="skill-box">
                            <h3>LANGUAGE SKILLS</h3>
                            <aside class="skill-language">
                                <div class="skill-row clearfix">
                                    <h4 class="skill-title clearfix"><img src="images/bl.png"
                                                                          alt=""/><span>BANGLA</span></h4>

                                    <div class="skill-data skill6"></div>
                                </div>
                            </aside>

                            <div class="textwidget">
                                <aside class="skill-language">
                                    <div class="skill-row clearfix">
                                        <h4 class="skill-title clearfix"><img src="images/en.png"
                                                                              alt=""/><span>ENGLISH</span></h4>

                                        <div class="skill-data skill4"></div>
                                    </div>
                                </aside>

                            </div>
                        </aside>
                    </div>
                </div>
            </div>

            <div class="item-border"><span></span></div>
        </section>
        <!-- /#resume -->

        <section id="portfolio" class="item noprint">
            <h2 class="item-title toggle closed"><span class="title">My Portfolio</span> <span
                    class="icon-portfolio"></span> <span class="arrow"></span></h2>

            <div class="item-cont">
                <div class="controls">
                    <ul>
                        <li class="filter active" data-filter="all">All</li>
                        <li class="filter" data-filter="category_20">Desktop Application</li>
                        <li class="filter" data-filter="category_18">Web Development</li>
                    </ul>
                </div>

                <ul id="Grid">
                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy1">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p1.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Noapara Group Bangladesh</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy1" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p1.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Noapara Group Bangladesh</h2>

                                    <p>NOAPARA GROUP BECOMING BANGLADESH'S LEADING FERTILIZER COMPANY</p>
                                    <p><a target="_blank" href="http://npgbd.com">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy2">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p3.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Lakshmipur Govt Mohila Collage</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy2" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p3.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Lakshmipur Govt Mohila Collage</h2>

<!--                                    <p>NOAPARA GROUP BECOMING BANGLADESH'S LEADING FERTILIZER COMPANY</p>-->
                                    <p><a target="_blank" href="http://lgmc-bd.com">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy3">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p5.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Aladadpur Rabbania Fazil Madrasha</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy3" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p5.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Aladadpur Rabbania Fazil Madrasha</h2>

<!--                                    <p>NOAPARA GROUP BECOMING BANGLADESH'S LEADING FERTILIZER COMPANY</p>-->
                                    <p><a target="_blank" href="http://aladadpurrabbaniafazilmadrasha.edu.bd/">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy4">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p2.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Raja Security</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy4" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p2.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Raja Security</h2>

<!--                                    <p>NOAPARA GROUP BECOMING BANGLADESH'S LEADING FERTILIZER COMPANY</p>-->
                                    <p><a target="_blank" href="http://rsgbd.com">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy5">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p4.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Lakshmipur Police</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy5" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p4.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Lakshmipur Police</h2>

                                    <p></p>
                                    <p><a target="_blank" href="http://splakshmipur.org">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy6">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p6.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Dynamic Software Ltd</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy6" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p6.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Dynamic Software Ltd</h2>

                                    <p></p>
                                    <p><a target="_blank" href="http://dynamicsoftwareltd.com">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy7">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p7.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>Shopper Collection</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy7" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p7.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>Shopper Collection</h2>

                                    <p></p>
                                    <p><a target="_blank" href="http://shopperscollection.com/">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy8">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p8.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>MSK Creation</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy8" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p8.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>MSK Creation</h2>

                                    <p></p>
                                    <p><a target="_blank" href="http://mskcreation.com">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mix mix_all category_18" data-cat="18">
                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">
                            <a class="fancybox" data-rel="group" href="#fancy9">
                                <div class="ptf-img-wrap">
                                    <img src="<?=base_url()?>assets/site/images/portfolio/p9.jpg" alt="" width="250px" height="180"/>
                                </div>
                                <div class="ptf-cover">
                                    <div class="ptf-button"><span>View Large</span></div>
                                    <div class="ptf-details">
                                        <h2>1 Stop Learning</h2>
                                        <span>Web Development</span>
                                    </div>
                                </div>
                            </a>

                            <div id="fancy9" class="fancy-wrap">
                                <img src="<?=base_url()?>assets/site/images/portfolio/p9.jpg" alt=""/>

                                <div class="fancy">
                                    <h2>1 Stop Learning</h2>

                                    <p>1 Stop learning is complete solution for manage your school activities. In this web application
                                    you can manage teacher and student, make course for student, online exam in that course for student, manage student attendance,
                                        examine student progress report, parents can view their child progress etc etc.
                                    </p>
                                    <p><a target="_blank" href="https://www.1stoplearning.org/">Go to Website</a></p>
                                </div>
                            </div>
                        </div>
                    </li>




                    <!--                    <li class="mix mix_all category_20" data-cat="20">-->
<!--                        <div class="ptf-item" data-itemid="328" data-type="fullslider" data-defwidth="350">-->
<!--                            <a class="fancybox" data-rel="group" href="#fancy5">-->
<!--                                <div class="ptf-img-wrap">-->
<!--                                    <img src="images/user_images/gallery/5-250x180.jpg" alt=""/>-->
<!--                                </div>-->
<!--                                <div class="ptf-cover">-->
<!--                                    <div class="ptf-button"><span>View Large</span></div>-->
<!--                                    <div class="ptf-details">-->
<!--                                        <h2>Cool Place</h2>-->
<!--                                        <span>Photography</span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </a>-->
<!---->
<!--                            <div id="fancy5" class="fancy-wrap">-->
<!--                                <img src="images/user_images/gallery/5.jpg" alt=""/>-->
<!---->
<!--                                <div class="fancy">-->
<!--                                    <h2>Cool Place</h2>-->
<!---->
<!--                                    <p>Perspiciatis unde omnis iste-->
<!--                                        natus error sit voluptatem accusantium doloremque laudantium, totam rem-->
<!--                                        aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto-->
<!--                                        beatae vitae dicta.</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </li>-->

            </div>

            <div class="item-border"><span></span></div>
        </section>
        <!-- /#portfolio -->

        <section id="contact" class="item noprint">
            <h2 class="item-title toggle closed"><span class="title">Contact info</span> <span
                    class="icon-contact"></span><span class="arrow"></span></h2>

            <div class="item-cont">
                <div class="map"><div id="map-canvas"></div></div>
                <div class="clearfix">
                    <div class="form col500 fl-left">
                        <h3>Let's keep in touch</h3>

                        <form action="" method="get" id="contact_form">
                            <div class="clearfix">
                                <div class="form-col form-marg small fl-left">
                                    <label>Name<span>*</span></label>
                                    <div class="field">
                                        <input class="form-item req" id="contact_name" name="contact_name" type="text">
                                    </div>
                                </div>
                                <div class="form-col small fl-left">
                                    <label>Email<span>*</span></label>

                                    <div class="field">
                                        <input class="form-item req" name="email" id="email" type="email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-col">
                                <label>Message<span>*</span></label>
                                <textarea id="comment" name="comment" class="form-item req"></textarea>
                            </div>
                            <div class="form-btn">
                                <div class="field">
                                    <input class="btn" name="submit" id="submit" value="send message" type="submit">
                                </div>
                            </div>
                        </form>
                        <div id="messages">&nbsp;</div>
                        <script type="text/javascript">
                            jQuery(document).ready(function () {
                                jQuery('#contact_form').ajaxForm({
                                    beforeSubmit: function () {
                                        return init_validation('#contact_form');
                                    },
                                    success: function () {
                                        alert('Your message has been sent!');
                                        jQuery('#contact_form').resetForm();
                                    }
                                });
                            });
                        </script>
                    </div>

                    <div class="col260 fl-right">
                        <h3>Contact info</h3>
                        <ul class="contact-info">
                            <li class="icon-home clearfix"><span class="icon"></span>
                                <span class="text">East Nandipara, Khilgoan, Dhaka-1214</span>
                            </li>
                            <li class="icon-phone clearfix"><span class="icon"></span>
                                <span class="text">+8801556984227</span>
                            </li>
                            <li class="icon-maile clearfix"><span class="icon"></span>
                                <span class="text">sohag2882@yahoo.com</span>
                            </li>
                            <li class="icon-world clearfix"><span class="icon"></span>
                                <span class="text">http://mustafiz.info</span>

                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="item-border"><span></span></div>
        </section>
        <!-- /#contact -->

        <a id="up"></a>
    </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>

    var map;
    var myLatlng = new google.maps.LatLng(23.745558, 90.451512);
    function initialize() {
        var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: '{{URL::asset('assets/site/images/map-pointer.png')}}',
            title: 'Portfolio marker'
    });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>


<!-- for quick loading -->
<img class="hide" src="images/bg/bg1.jpg" alt=""/>
<img class="hide" src="images/bg/bg2.jpg" alt=""/>
<img class="hide" src="images/bg/bg3.jpg" alt=""/>
<img class="hide" src="images/bg/bg4.jpg" alt=""/>
<img class="hide" src="images/bg/bg5.jpg" alt=""/>
<img class="hide" src="images/bg/bg6.jpg" alt=""/>
<img class="hide" src="images/bg/bg7.jpg" alt=""/>
<img class="hide" src="images/bg/bg8.jpg" alt=""/>
<img class="hide" src="images/bg/bg9.jpg" alt=""/>
<img class="hide" src="images/bg/bg10.jpg" alt=""/>
<img class="hide" src="images/bg/bg11.jpg" alt=""/>
<img class="hide" src="images/bg/bg12.jpg" alt=""/>

</body>
</html>