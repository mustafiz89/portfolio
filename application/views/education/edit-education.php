
<?php
$this->load->view('layouts/dashboard-left-menu');
?>
<!--leftmenu-->

<div class="centercontent">
    <div id="contentwrapper" class="contentwrapper">
        <!--all content goes here-->
        <div class="pageheader">
            <h1 class="pagetitle"><?=$title?></h1>
            <br>
        </div>
        <!--pageheader-->
        <div id="contentwrapper" class="contentwrapper">
            <div id="basicform" class="subcontent">
                <?php
                $this->load->view('layouts/add-message');
                ?>

                <!--contenttitle-->

                <form class="stdform" action="<?=base_url()?>education/update" method="post">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id" value="<?=$data->education_id?>" />
                    <p>
                        <label>Degree</label>
                        <span class="field"><input type="text" name="degree" class="mediuminput" required value="<?=$data->degree?>" /></span>
                    </p>

                    <p>
                        <label>Institute</label>
                        <span class="field"><input type="text" name="institute" class="mediuminput" required value="<?=$data->institute?>" /></span>
                    </p>

                    <p>
                        <label>Result</label>
                        <span class="field"><input type="text" name="result" class="smallinput" required value="<?=$data->result?>" /></span>
                    </p>
                    <?php if($data->group)
                    {
                        ?>
                    <p class="group">
                        <label>Group</label>
                        <span class="field"><input type="text" name="group" class="mediuminput" value="<?=$data->group?>" /></span>
                    </p>
                    <?php }
                    else if($data->subject)
                    {?>

                    <p class="subject">
                        <label>Subject</label>
                        <span class="field"><input type="text" name="subject" class="mediuminput" value="<?=$data->subject?>" /></span>
                    </p>
                    <?php }?>
                    <p>
                        <label>Board</label>
                        <span class="field"><input type="text" name="board" class="smallinput" required value="<?=$data->board?>" /></span>
                    </p>

                    <p>
                        <label>Duration</label>
                        <span class="field"><input type="text" name="duration" class="smallinput" required value="<?=$data->duration?>"/></span>
                    </p>

                    <p class="stdformbutton">
                        <button class="stdbtn btn_orange">Edit Information</button>
                    </p>
                </form>

            </div><!-- #activities -->
        </div><!--contentwrapper-->
        <br clear="all" />
    </div>
    <!--contentwrapper-->
    <br clear="all" />
</div><!-- centercontent -->
<script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
<script>
    $(function(){
        $('.msg').delay(3000).fadeOut('Slow');
    });
</script>
