<?php
$this->load->view('layouts/dashboard-left-menu');
?>
<!--leftmenu-->
<div class="centercontent">

    <div class="pageheader">
        <h1 class="pagetitle"><?= $title; ?></h1>
        <br>
    </div>
    <!--pageheader-->
    <div id="contentwrapper" class="contentwrapper">
        <!--all content goes here-->
        <div id="basicform" class="subcontent">
            <?php
            $this->load->view('layouts/edit-message');
            ?>
            <!--contenttitle-->
            <br>

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable">

                <thead>
                <tr>
                    <th class="">ID</th>
                    <th class="">Degree</th>
                    <th class="">Institute</th>
                    <th class="">Result</th>
                    <th class="">Group</th>
                    <th class="">Subject</th>
                    <th class="">Board</th>
                    <th class="">Duration</th>

                    <th class="" style="width:10%">Action</th>
                </tr>
                </thead>


                <tbody>
                <?php
                $i=0;
                foreach($allData as $data)
                {
                    $i+=1;
                    ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $data->degree ?></td>
                    <td><?= $data->institute ?></td>
                    <td><?= $data->result ?></td>
                    <td><?= $data->group ?></td>
                    <td><?= $data->subject?></td>
                    <td><?= $data->board ?></td>
                    <td><?= $data->duration ?></td>

                    <td>
                        <a href="<?= base_url() ?>education/edit/<?=$data->education_id ?>"
                           class="btn btn4 btn_blue btn_pencil radius50" title="Edit"></a>
                        <a href="<?= base_url() ?>education/destroy/<?=$data->education_id ?>"
                           class="btn btn4 btn_orange btn_trash radius50" title="Delete" onclick="return checkDelete()"></a>

                    </td>
                </tr>
                <?php }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <!--contentwrapper-->
    <br clear="all"/>
</div><!-- centercontent -->
<script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
<script>
    $(function(){
        $('.msg').delay(2000).fadeOut('slow');
    });
</script>
