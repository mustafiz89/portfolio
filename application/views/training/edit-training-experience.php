
<?php
$this->load->view('layouts/dashboard-left-menu');
?>
<!--leftmenu-->

<div class="centercontent">
    <div id="contentwrapper" class="contentwrapper">
        <!--all content goes here-->
        <div class="pageheader">
            <h1 class="pagetitle"><?=$title?></h1>
            <br>
        </div>
        <!--pageheader-->
        <div id="contentwrapper" class="contentwrapper">
            <div id="basicform" class="subcontent">
                <?php
                $this->load->view('layouts/add-message');
                ?>

                <!--contenttitle-->

                <form class="stdform" action="<?=base_url()?>training/update" method="post">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                    <input type="hidden" name="id" value="<?=$data->training_id?>" />
                    <p>
                        <label>Job Title</label>
                        <span class="field"><input type="text" name="title" class="mediuminput" required value="<?=$data->title?>" /></span>
                    </p>

                    <p>
                        <label>Company Name</label>
                        <span class="field"><input type="text" name="organization" class="mediuminput" required value="<?=$data->organization?>"/></span>
                    </p>

                    <p>
                        <label>Duration</label>
                        <span class="field"><input type="text" name="duration" class="smallinput" required value="<?=$data->duration?>"/></span>
                    </p>
                    <p>
                        <label>Description</label>
                        <span class="field">
                            <textarea cols="80" rows="5" class="longinput" name="description" required><?=$data->description?></textarea>
                        </span>
                    </p>

                    <p class="stdformbutton">
                        <button class="stdbtn btn_red">Edit Information</button>
                    </p>
                </form>

            </div><!-- #activities -->
        </div><!--contentwrapper-->
        <br clear="all" />
    </div>
    <!--contentwrapper-->
    <br clear="all" />
</div><!-- centercontent -->
<script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
<script>
    $(function(){
        $('.msg').delay(3000).fadeOut('Slow');
    });
</script>
