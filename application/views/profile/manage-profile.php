<?php
$this->load->view('layouts/dashboard-left-menu');
?>
<!--leftmenu-->
<div class="centercontent">

    <div class="pageheader">
        <h1 class="pagetitle"><?= $title; ?></h1>
        <br>
    </div>
    <!--pageheader-->
    <div id="contentwrapper" class="contentwrapper">
        <!--all content goes here-->
        <div id="basicform" class="subcontent">
            <?php
            $this->load->view('layouts/edit-message');
            ?>
            <!--contenttitle-->
            <br>

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable">

                <thead>
                <tr>
                    <th class="">FullName</th>
                    <th class="">Nickname</th>
                    <th class="">Age</th>
                    <th class="">Address</th>
                    <th class="">Email</th>
                    <th class="">Phone</th>
                    <th class="">Freelance</th>
                    <th class="">Title</th>
                    <th class="">Description</th>
                    <th class="">Action</th>
                </tr>
                </thead>


                <tbody>
                <!--        --><?php //foreach($alldata as $data){?>
                <tr>
                    <td><?= $data->fullname ?></td>
                    <td><?= $data->nickname ?></td>
                    <td><?= $data->age ?></td>
                    <td><?= $data->address ?></td>
                    <td><?= $data->email ?></td>
                    <td><?= $data->phone ?></td>
                    <td><?= $data->freelance ?></td>
                    <td><?= $data->title ?></td>
                    <td><?= $data->description ?></td>

                    <td>
                        <a href="<?= base_url() ?>profile/edit/<?= $data->profile_id ?>"
                           class="btn btn4 btn_blue btn_pencil radius50" title="Edit"></a>
                        <!--                        <a href="-->
                        <!--" class="btn btn4 btn_orange btn_trash radius50" title="Delete" onclick="return checkDelete()"></a>-->

                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <!--contentwrapper-->
    <br clear="all"/>
</div><!-- centercontent -->
<script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
<script>
    $(function(){
        $('.msg').delay(2000).fadeOut('slow');
    });
</script>
