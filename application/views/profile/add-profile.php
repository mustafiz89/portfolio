
<?php
$this->load->view('layouts/dashboard-left-menu');
?>
<!--leftmenu-->

<div class="centercontent">
    <div id="contentwrapper" class="contentwrapper">
        <!--all content goes here-->
        <div class="pageheader">
            <h1 class="pagetitle"><?=$title?></h1>
            <br>
        </div>
        <!--pageheader-->
        <div id="contentwrapper" class="contentwrapper">
            <div id="basicform" class="subcontent">

                <div class="message">
                    <h3></h3>
                </div><!--contenttitle-->

                <form class="stdform" action="<?=base_url()?>profile/store" method="post">
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                    <p>
                        <label>Full Name</label>
                        <span class="field"><input type="text" name="fullname" class="mediuminput" required /></span>
                    </p>

                    <p>
                        <label>Nickname</label>
                        <span class="field"><input type="text" name="nickname" class="mediuminput" required /></span>
                    </p>

                    <p>
                        <label>Age</label>
                        <span class="field"><input type="text" name="age" class="smallinput" required /></span>
                    </p>

                    <p>
                        <label>Address</label>
                        <span class="field"><input type="text" name="address" class="mediuminput" required /></span>
                    </p>

                    <p>
                        <label>Email</label>
                        <span class="field"><input type="text" name="email" class="smallinput" required /></span>
                    </p>

                    <p>
                        <label>Phone</label>
                        <span class="field"><input type="text" name="phone" class="smallinput" required /></span>
                    </p>

                    <p>
                        <label>Title</label>
                        <span class="field"><input type="text" name="title" class="mediuminput" required /></span>
                    </p>


                    <p>
                        <label>Description</label>
                        <span class="field">
                            <textarea cols="80" rows="5" class="longinput" name="description" required></textarea>
                        </span>
                    </p>

                    <p>
                        <label>Freelance</label>
                            <span class="field">
                            <select name="freelance" class="uniformselect" required>
                                <option value="">---Choose One---</option>
                                <option value="available">Available</option>
                                <option value="not available">Not Available</option>
                            </select>
                            </span>
                    </p>


                    <p>
                        <label>File Upload</label>
                            <span class="field">
                            	<input type="file" name="image" required />
                            </span>
                    </p>

                    <p class="stdformbutton">
                        <button class="stdbtn btn_blue">Add Information</button>
                    </p>
                </form>

            </div><!-- #activities -->
        </div><!--contentwrapper-->
        <br clear="all" />
    </div>
    <!--contentwrapper-->
    <br clear="all" />
</div><!-- centercontent -->