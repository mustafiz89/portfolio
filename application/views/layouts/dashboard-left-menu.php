
<div class="vernav2 iconmenu">
    <ul>
        <li><a href="#formsub" class="editor">My Profile</a>
            <span class="arrow"></span>
            <ul id="formsub">
                <li><a href="<?=base_url()?>profile/add">Add Information</a></li>
                <li><a href="<?=base_url()?>profile/view">Manage Information</a></li>
            </ul>
        </li>
        <li><a href="#error" class="editor">Education</a>
            <span class="arrow"></span>
            <ul id="error">
                <li><a href="<?=base_url()?>education/add">Add Information</a></li>
                <li><a href="<?=base_url();?>education">Manage Information</a></li>
            </ul>
        </li>
        <li><a href="#calender" class="editor">Professional experience</a>
            <span class="arrow"></span>
            <ul id="calender">
                <li><a href="<?=base_url()?>experience/add">Add Information</a></li>
                <li><a href="<?=base_url()?>experience">Manage Information</a></li>
            </ul>
        </li>
        <li><a href="#widget" class="editor">Training Experience</a>
            <span class="arrow"></span>
            <ul id="widget">
                <li><a href="<?=base_url()?>training/add">Add Information</a></li>
                <li><a href="<?=base_url()?>training/index">Mangae Information</a></li>
            </ul>
        </li>
        <li><a href="#resume" class="editor">Resume</a>
            <span class="arrow"></span>
            <ul id="resume">
                <li><a href="">Manage Resume</a></li>
            </ul>
        </li>
        <li><a href="#wp" class="editor">Web Application Portfolio</a>
            <span class="arrow"></span>
            <ul id="wp">
                <li><a href="">Add Information</a></li>
                <li><a href="">Manage Information</a></li>
            </ul>
        </li>
        <li><a href="#dp" class="editor">Desktop Application Portfolio</a>
            <span class="arrow"></span>
            <ul id="dp">
                <li><a href="">Add Information</a></li>
                <li><a href="">Manage Information</a></li>
            </ul>
        </li>
        <li><a href="#ci" class="editor">Contact Info</a>
            <span class="arrow"></span>
            <ul id="ci">
                <li><a href="">Add Information</a></li>
                <li><a href="">Manage Information</a></li>
            </ul>
        </li>

    </ul>
    <a class="togglemenu"></a>
    <br /><br />
</div><!--leftmenu-->


