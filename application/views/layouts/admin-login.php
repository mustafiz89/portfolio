<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">

    <title>Login</title>

    <link rel="shortcut icon" href="<?=base_url();?>assets/admin/images/favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/bootstrap.min.css?v2.0.0">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/bootstrap-extend.min.css?v2.0.0">
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/site.min.css?v2.0.0">
    <!-- Page -->
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/login-v2.min.css?v2.0.0'">
    <!-- Fonts -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
</head>
<body class="page-login-v2 layout-full page-dark">
<!-- Page -->
<div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content">
        <div class="page-brand-info">
            <div class="brand">
                <h2 class="brand-text font-size-40"> Mustafizur Rahman</h2>

                <p class="font-size-20">The Success Key Is only On Your Hand</p>
            </div>
        </div>

        <div class="page-login-main">
            <div class="brand visible-xs">
                <h3 class="brand-text font-size-40">Remark</h3>
            </div>
            <h3 class="font-size-24">Sign In</h3>

            <p>Please sign in with your email and password</p>

            <form method="post" action="<?=base_url()?>Login/checkLoginCredentials">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="form-group">
                    <label class="sr-only" for="inputEmail">Email</label>
                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="inputPassword">Password</label>
                    <input type="password" class="form-control" id="inputPassword" name="password"
                           placeholder="Password">
                </div>
                <div class="form-group clearfix">
                    <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
                        <input type="checkbox" id="remember" name="checkbox">
                        <label for="inputCheckbox">Remember me</label>
                    </div>
                    <a class="pull-right" href="">Forgot password?</a>
                </div>
                <button class="btn btn-success btn-block btn-sm hidden"><b>Login Successful.Redirecting....</b></button>
                <button class="btn btn-info btn-block btn-sm hidden"><b>Authenticating....</b></button>
                <button class="btn btn-info btn-block btn-sm "><b>Sign in</b></button>
                <button class="btn btn-danger btn-block btn-sm hidden"><b>Login Error. Please try again!!</b></button>


            </form>

            <p>No account? <a href="">Sign Up</a></p>

            <footer class="page-copyright">
                <p>Developed By Mustafizur Rahman</p>

                <p>© 2015. All RIGHT RESERVED.</p>
            </footer>
        </div>

    </div>
</div>
<!-- End Page -->
</body>

</html>