<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Mirrored from demo.themepixels.com/webpage/amanda/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Nov 2015 06:52:51 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="<?=base_url();?>assets/admin/css/style.default.css" type="text/css" />
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/plugins/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/custom/general.js"></script>
    <script type="text/javascript" src="<?=base_url();?>assets/admin/js/custom/index.js"></script>
    <script type="text/javascript">
        function checkDelete(){
            var check=confirm('Are You Sure Want To Delete It?')
            if(check){
                return true;
            }
            else{
                return false;
            }

        }
    </script>

</head>
<body class="withvernav">

<div class="bodywrapper">
    <div class="topheader">
        <div class="left">
            <h1 class="logo">Musatafizur Rahman</h1>

            <br clear="all" />

        </div><!--left-->

        <div class="right">
            <div class="userinfo">
                <img src="images/thumbs/avatar.png" alt="" />
                <span>Juan Dela Cruz</span>
            </div><!--userinfo-->

            <div class="userinfodrop">
                <div class="avatar">
                    <a href="#"><img src="images/thumbs/avatarbig.png" alt="" /></a>
                </div><!--avatar-->
                <div class="userdata">
                    <h4>Juan Dela Cruz</h4>
                    <span class="email">youremail@yourdomain.com</span>
                    <ul>
                        <li><a href="">Edit Profile</a></li>
                        <li><a href="">Account Settings</a></li>
                        <li><a href="">Help</a></li>
                        <li><a href="">Sign Out</a></li>
                    </ul>
                </div><!--userdata-->
            </div><!--userinfodrop-->
        </div><!--right-->
    </div><!--topheader-->


    <div class="header">
        <ul class="headermenu">
            <li class="<?=base_url()?>"><a href=""><span class="icon icon-flatscreen"></span>Dashboard</a></li>
            <li><a href=""><span class="icon icon-pencil"></span>Manage Blog</a></li>
            <li><a href=""><span class="icon icon-message"></span>Messages</a></li>
            <li><a href=""><span class="icon icon-chart"></span>Reports</a></li>
        </ul>
    </div>
    <!--header-->
    <?=$content?>



</div><!--bodywrapper-->

</body>



</html>
